This repo demonstrates how to build a container image in GitLab CI, using
podman or buildah. Due to lack of support for nftables in the host OS, we need
to configure the system to use iptables instead.

The workarounds were still needed with Google COS-105 and kernel 5.15.109+, but
they are no longer needed with the latest kernel 5.15.154+ that just landed a
few days ago.

It seems that we benefit from this change:
    
> Added kernel compatibility with iptables-nft.

Therefore this repo is now OBSOLETE!

References:
- <https://cloud.google.com/container-optimized-os/docs/release-notes/m105#January_31_2024>
- <https://about.gitlab.com/blog/2023/10/04/updating-the-os-version-of-saas-runners-on-linux/>
